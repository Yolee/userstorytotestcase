﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserStoryToTestCase
{
    public class TestCaseManager
    {
        public string Url { get; set; }
        public string Keyword { get; set; }
        public string Function { get; set; }
        public string XPath { get; set; }
        public string Data { get; set; }
        public string CheckURL { get; set; }
        public string UserStory { get; set; }
        public List<StringBuilder> TestCasesCodeList { get; set; }
        public List<string> TestCasesList { get; set; }

        private List<string> _steps;

        public TestCaseManager()
        {
            _steps = new List<string>();
            TestCasesList = new List<string>();
            TestCasesCodeList = new List<StringBuilder>();
        }

        public void AddStep(string step)
        {
            _steps.Add(step);
        }

        public bool DeleteLastStep(IList steps)
        {
            if (steps.Count < 1)
            {
                return false;
            }

            steps.RemoveAt(steps.Count - 1);
            return true;
        }

        public string GenerateStep()
        {
            string step = "";     

            if (Function == "Press Button")
            {
                step = $"{Keyword} {Function} with XPath: {XPath}";
            }
            else if (Function == "Fill the Field")
            {
                step = $"{Keyword} {Function} at XPath: {XPath} with {Data}";
            }
            else if (Function == "Check URL")
            {
                step = $"{Keyword} {Function} {CheckURL}";
            }
            else if (Function == "Check output")
            {
                step = $"{Keyword} {Function} at XPath: {XPath} equals {Data}";
            }

            return step;
        }

        public string AddFirstStep(int count)
        {
            string step = "";

            if (IsFirstStep(count))
            {
                step = $"GIVEN I am on the URL \"{Url}\"";
            }

            return step;
        }

        public void RemoveLastTestCase()
        {
            TestCasesCodeList.Remove(TestCasesCodeList.Last());
            TestCasesList.Remove(TestCasesList.Last());
        }

        public string GenerateTestCode(string url, IList steps)
        {
            GenerateTestCaseCode(url, steps);
            if (url is null)
            {
                throw new ArgumentNullException(nameof(url));
            }

            StringBuilder testCode = new StringBuilder();
  
            testCode.Append(GenerateTestCodeStart());

            for (int i = 0; i < TestCasesCodeList.Count; i++)
            {
                testCode.Append(TestCasesCodeList[i]);
            }

            testCode.Append(GenerateTestCodeEnd());

            return testCode.ToString();
        }

        public StringBuilder GenerateTestCodeEnd()
        {
            StringBuilder testCode = new StringBuilder();

            testCode.AppendLine("        [TearDown]");
            testCode.AppendLine("        public void TearDown()");
            testCode.AppendLine("        {");
            testCode.AppendLine("            _driver.Quit();");
            testCode.AppendLine("        }");
            testCode.AppendLine("    }");
            testCode.AppendLine("}");

            return testCode;
        }

        public void GenerateTestCaseCode(string url, IList steps)
        {
            StringBuilder testCode = new StringBuilder();

            if (url is null)
            {
                throw new ArgumentNullException(nameof(url));
            }

            testCode.AppendLine("        [Test]");
            testCode.AppendLine($"        public void Test_{UserStory.Replace(" ", "_").Replace(",", "")}{TestCasesList.Count + 1}()");
            testCode.AppendLine("        {");
            testCode.AppendLine($"            _driver.Navigate().GoToUrl(\"{Url}\");");
            testCode.AppendLine("            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(10));");

            foreach (string step in steps)
            {
                string[] parts = step.Split(' ');
                string function = parts[1];

                if (function == "Press")
                {
                    string xpath = parts.Length > 5 ? parts[5] : "";
                    testCode.AppendLine($"            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(\"{xpath}\")));");
                    testCode.AppendLine($"            _driver.FindElement(By.XPath(\"{xpath}\")).Click();");
                }
                else if (function == "Fill")
                {
                    string xpath = parts.Length > 7 ? parts[parts.Length - 3] : "";
                    string data = parts.Length > 7 ? parts[parts.Length - 1] : "";
                    testCode.AppendLine($"            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(\"{xpath}\")));");
                    testCode.AppendLine($"            _driver.FindElement(By.XPath(\"{xpath}\")).SendKeys(\"{data}\");");
                }
                else if (function == "Check")
                {
                    if (parts[3] == "URL")
                    {
                        string URL = parts[3];
                        testCode.AppendLine($"            wait.Until(driver => driver.Url == \"{URL}\");");
                    }
                    else if (parts[2] == "output")
                    {
                        string xpath = parts.Length > 5 ? parts[5] : "";
                        string inputData = parts.Length > 7 ? parts[7] : "";
                        testCode.AppendLine($"            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(\"{xpath}\")));");
                        testCode.AppendLine($"            string outputData = _driver.FindElement(By.XPath(\"{xpath}\")).Text;");
                        testCode.AppendLine($"            Assert.AreEqual(\"{inputData}\", outputData, \"Output data does not match the input data.\");");
                    }
                }
            }

            testCode.AppendLine("        }");
            testCode.AppendLine();

            TestCasesCodeList.Add(testCode);
        }

        public StringBuilder GenerateTestCodeStart()
        {
            StringBuilder testCode = new StringBuilder();

            testCode.AppendLine("using NUnit.Framework;");
            testCode.AppendLine("using OpenQA.Selenium;");
            testCode.AppendLine("using OpenQA.Selenium.Chrome;");
            testCode.AppendLine("using OpenQA.Selenium.Support.UI;");
            testCode.AppendLine("using System;");
            testCode.AppendLine();
            testCode.AppendLine("namespace BDDTest");
            testCode.AppendLine("{");
            testCode.AppendLine("    [TestFixture]");
            testCode.AppendLine("    public class SeleniumTest");
            testCode.AppendLine("    {");
            testCode.AppendLine("        private IWebDriver _driver;");
            testCode.AppendLine();
            testCode.AppendLine("        [SetUp]");
            testCode.AppendLine("        public void Setup()");
            testCode.AppendLine("        {");
            testCode.AppendLine("            _driver = new ChromeDriver();");
            testCode.AppendLine("        }");
            testCode.AppendLine();

            return testCode;
        }

        public string GenerateTestCasesList(IList steps)
        {
            StringBuilder testCase = new StringBuilder();
            StringBuilder test = new StringBuilder();

            test.AppendLine($"Vartotojo istorija: {UserStory}");
            test.AppendLine();
            test.AppendLine("Testinių atvejų sąrašas:");
            test.AppendLine();

            foreach (string step in steps)
            {
                testCase.AppendLine(step);
            }

            TestCasesList.Add(testCase.ToString());

            for (int i = 0; i < TestCasesList.Count; i++)
            {
                test.AppendLine($"{i + 1}-as testinis atvejis:");
                test.AppendLine();
                test.AppendLine(TestCasesList[i]);
            }

            return test.ToString();
        }

        public bool IsStepValid()
        {
            if (Function == "Press Button" || Function == "Fill the Field" || Function == "Check output")
            {
                if (string.IsNullOrWhiteSpace(XPath))
                {
                    return false;
                }
            }

            if (Function == "Fill the Field" || Function == "Check output")
            {
                if (string.IsNullOrWhiteSpace(Data))
                {
                    return false;
                }
            }

            if (Function == "Check URL")
            {
                if (string.IsNullOrWhiteSpace(CheckURL))
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsTestCaseValid(string keyword)
        {
            if (keyword != "AND")
            {
                return false;
            }

            return true;
        }

        public bool IsFirstStep(int count)
        {
            if (count == 0)
            {
                return true;
            }

            return false;
        }

        public List<string> GetSteps()
        {
            return _steps;
        }

        public int GetStepCount()
        {
            return _steps.Count;
        }
    }
}

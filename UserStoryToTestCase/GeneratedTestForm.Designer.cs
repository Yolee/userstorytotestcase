﻿namespace UserStoryToTestCase
{
    partial class GeneratedTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbGeneratedTestCode = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBackToTestCase = new System.Windows.Forms.Button();
            this.btnDownloadTest = new System.Windows.Forms.Button();
            this.btnStartNewTestCase = new System.Windows.Forms.Button();
            this.btnStartNewUserStory = new System.Windows.Forms.Button();
            this.lblGeneratedTestCase = new System.Windows.Forms.Label();
            this.rtbGeneratedTestCase = new System.Windows.Forms.RichTextBox();
            this.btnSaveTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtbGeneratedTestCode
            // 
            this.rtbGeneratedTestCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbGeneratedTestCode.Location = new System.Drawing.Point(19, 107);
            this.rtbGeneratedTestCode.Margin = new System.Windows.Forms.Padding(10);
            this.rtbGeneratedTestCode.Name = "rtbGeneratedTestCode";
            this.rtbGeneratedTestCode.Size = new System.Drawing.Size(371, 419);
            this.rtbGeneratedTestCode.TabIndex = 0;
            this.rtbGeneratedTestCode.Text = "";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 65);
            this.label1.Margin = new System.Windows.Forms.Padding(10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Testo kodas";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnBackToTestCase
            // 
            this.btnBackToTestCase.AutoSize = true;
            this.btnBackToTestCase.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackToTestCase.Location = new System.Drawing.Point(19, 19);
            this.btnBackToTestCase.Margin = new System.Windows.Forms.Padding(10);
            this.btnBackToTestCase.Name = "btnBackToTestCase";
            this.btnBackToTestCase.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnBackToTestCase.Size = new System.Drawing.Size(77, 28);
            this.btnBackToTestCase.TabIndex = 13;
            this.btnBackToTestCase.Text = "Atgal";
            this.btnBackToTestCase.UseVisualStyleBackColor = true;
            this.btnBackToTestCase.Click += new System.EventHandler(this.BtnBackToTestCase_Click);
            // 
            // btnDownloadTest
            // 
            this.btnDownloadTest.AutoSize = true;
            this.btnDownloadTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDownloadTest.Location = new System.Drawing.Point(112, 546);
            this.btnDownloadTest.Margin = new System.Windows.Forms.Padding(10);
            this.btnDownloadTest.Name = "btnDownloadTest";
            this.btnDownloadTest.Size = new System.Drawing.Size(158, 28);
            this.btnDownloadTest.TabIndex = 14;
            this.btnDownloadTest.Text = "Išsaugoti kodą";
            this.btnDownloadTest.UseVisualStyleBackColor = true;
            this.btnDownloadTest.Click += new System.EventHandler(this.BtnDownloadTest_Click);
            // 
            // btnStartNewTestCase
            // 
            this.btnStartNewTestCase.AutoSize = true;
            this.btnStartNewTestCase.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartNewTestCase.Location = new System.Drawing.Point(602, 614);
            this.btnStartNewTestCase.Margin = new System.Windows.Forms.Padding(10);
            this.btnStartNewTestCase.Name = "btnStartNewTestCase";
            this.btnStartNewTestCase.Size = new System.Drawing.Size(159, 28);
            this.btnStartNewTestCase.TabIndex = 16;
            this.btnStartNewTestCase.Text = "Pridėti testinį atvejį";
            this.btnStartNewTestCase.UseVisualStyleBackColor = true;
            this.btnStartNewTestCase.Click += new System.EventHandler(this.BtnStartNewTestCase_Click);
            // 
            // btnStartNewUserStory
            // 
            this.btnStartNewUserStory.AutoSize = true;
            this.btnStartNewUserStory.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartNewUserStory.Location = new System.Drawing.Point(700, 19);
            this.btnStartNewUserStory.Margin = new System.Windows.Forms.Padding(10);
            this.btnStartNewUserStory.Name = "btnStartNewUserStory";
            this.btnStartNewUserStory.Size = new System.Drawing.Size(165, 28);
            this.btnStartNewUserStory.TabIndex = 17;
            this.btnStartNewUserStory.Text = "Naujas testas";
            this.btnStartNewUserStory.UseVisualStyleBackColor = true;
            this.btnStartNewUserStory.Click += new System.EventHandler(this.BtnStartNewUserStory_Click);
            // 
            // lblGeneratedTestCase
            // 
            this.lblGeneratedTestCase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGeneratedTestCase.AutoSize = true;
            this.lblGeneratedTestCase.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGeneratedTestCase.Location = new System.Drawing.Point(494, 65);
            this.lblGeneratedTestCase.Margin = new System.Windows.Forms.Padding(10);
            this.lblGeneratedTestCase.Name = "lblGeneratedTestCase";
            this.lblGeneratedTestCase.Size = new System.Drawing.Size(71, 22);
            this.lblGeneratedTestCase.TabIndex = 19;
            this.lblGeneratedTestCase.Text = "Testas";
            this.lblGeneratedTestCase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rtbGeneratedTestCase
            // 
            this.rtbGeneratedTestCase.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbGeneratedTestCase.Location = new System.Drawing.Point(494, 107);
            this.rtbGeneratedTestCase.Margin = new System.Windows.Forms.Padding(10);
            this.rtbGeneratedTestCase.Name = "rtbGeneratedTestCase";
            this.rtbGeneratedTestCase.Size = new System.Drawing.Size(371, 419);
            this.rtbGeneratedTestCase.TabIndex = 18;
            this.rtbGeneratedTestCase.Text = "";
            // 
            // btnSaveTest
            // 
            this.btnSaveTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveTest.Location = new System.Drawing.Point(613, 546);
            this.btnSaveTest.Name = "btnSaveTest";
            this.btnSaveTest.Size = new System.Drawing.Size(133, 28);
            this.btnSaveTest.TabIndex = 20;
            this.btnSaveTest.Text = "Išsaugoti testą";
            this.btnSaveTest.UseVisualStyleBackColor = true;
            this.btnSaveTest.Click += new System.EventHandler(this.btnSaveTest_Click);
            // 
            // GeneratedTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 661);
            this.Controls.Add(this.btnSaveTest);
            this.Controls.Add(this.lblGeneratedTestCase);
            this.Controls.Add(this.rtbGeneratedTestCase);
            this.Controls.Add(this.btnStartNewUserStory);
            this.Controls.Add(this.btnStartNewTestCase);
            this.Controls.Add(this.btnDownloadTest);
            this.Controls.Add(this.btnBackToTestCase);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtbGeneratedTestCode);
            this.Name = "GeneratedTestForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GeneratedTestForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbGeneratedTestCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBackToTestCase;
        private System.Windows.Forms.Button btnDownloadTest;
        private System.Windows.Forms.Button btnStartNewTestCase;
        private System.Windows.Forms.Button btnStartNewUserStory;
        private System.Windows.Forms.Label lblGeneratedTestCase;
        private System.Windows.Forms.RichTextBox rtbGeneratedTestCase;
        private System.Windows.Forms.Button btnSaveTest;
    }
}
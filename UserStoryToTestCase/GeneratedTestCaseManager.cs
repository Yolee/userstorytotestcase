﻿using System;
using System.IO;
using System.Windows.Forms;

namespace UserStoryToTestCase
{
    public class GeneratedTestCaseManager
    {
        public void SaveFile(string generatedTestCode)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "C# Files (*.cs)|*.cs",
                DefaultExt = "cs",
                AddExtension = true
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, generatedTestCode);
                MessageBox.Show("Testo kodas išsaugotas sėkmingai.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void SaveTestFile(string generatedTestCase)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                Filter = "Text Files (*.txt)|*.txt",
                DefaultExt = "txt",
                AddExtension = true
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                {
                    writer.Write(generatedTestCase);
                }

                MessageBox.Show("Testas išsaugotas sėkmingai", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}

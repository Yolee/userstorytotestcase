﻿namespace UserStoryToTestCase
{
    partial class TestCaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserStory = new System.Windows.Forms.Label();
            this.lblURL = new System.Windows.Forms.Label();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.cbKeyword = new System.Windows.Forms.ComboBox();
            this.cbFunction = new System.Windows.Forms.ComboBox();
            this.lblXPath = new System.Windows.Forms.Label();
            this.txtXPath = new System.Windows.Forms.TextBox();
            this.lbSteps = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddStep = new System.Windows.Forms.Button();
            this.lblData = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.TextBox();
            this.btnGenerateTestCode = new System.Windows.Forms.Button();
            this.txtCheckURL = new System.Windows.Forms.TextBox();
            this.lblCheckURL = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDeleteLastStep = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vartotojo Istorija";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUserStory
            // 
            this.lblUserStory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserStory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUserStory.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserStory.Location = new System.Drawing.Point(23, 56);
            this.lblUserStory.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
            this.lblUserStory.Name = "lblUserStory";
            this.lblUserStory.Size = new System.Drawing.Size(838, 23);
            this.lblUserStory.TabIndex = 1;
            // 
            // lblURL
            // 
            this.lblURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblURL.AutoSize = true;
            this.lblURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblURL.Location = new System.Drawing.Point(549, 103);
            this.lblURL.Margin = new System.Windows.Forms.Padding(10);
            this.lblURL.Name = "lblURL";
            this.lblURL.Size = new System.Drawing.Size(128, 22);
            this.lblURL.TabIndex = 2;
            this.lblURL.Text = "Pradinis URL";
            this.lblURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtURL
            // 
            this.txtURL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtURL.Location = new System.Drawing.Point(553, 140);
            this.txtURL.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(308, 24);
            this.txtURL.TabIndex = 5;
            // 
            // cbKeyword
            // 
            this.cbKeyword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbKeyword.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbKeyword.FormattingEnabled = true;
            this.cbKeyword.Items.AddRange(new object[] {
            "WHEN",
            "AND"});
            this.cbKeyword.Location = new System.Drawing.Point(671, 175);
            this.cbKeyword.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.cbKeyword.Name = "cbKeyword";
            this.cbKeyword.Size = new System.Drawing.Size(190, 26);
            this.cbKeyword.TabIndex = 6;
            // 
            // cbFunction
            // 
            this.cbFunction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFunction.FormattingEnabled = true;
            this.cbFunction.Items.AddRange(new object[] {
            "Press Button",
            "Fill the Field",
            "Check URL",
            "Check output"});
            this.cbFunction.Location = new System.Drawing.Point(671, 217);
            this.cbFunction.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.cbFunction.Name = "cbFunction";
            this.cbFunction.Size = new System.Drawing.Size(190, 26);
            this.cbFunction.TabIndex = 7;
            // 
            // lblXPath
            // 
            this.lblXPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblXPath.AutoSize = true;
            this.lblXPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXPath.Location = new System.Drawing.Point(551, 263);
            this.lblXPath.Margin = new System.Windows.Forms.Padding(10);
            this.lblXPath.Name = "lblXPath";
            this.lblXPath.Size = new System.Drawing.Size(64, 22);
            this.lblXPath.TabIndex = 8;
            this.lblXPath.Text = "XPath";
            this.lblXPath.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtXPath
            // 
            this.txtXPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtXPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXPath.Location = new System.Drawing.Point(553, 300);
            this.txtXPath.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.txtXPath.Name = "txtXPath";
            this.txtXPath.Size = new System.Drawing.Size(293, 24);
            this.txtXPath.TabIndex = 9;
            // 
            // lbSteps
            // 
            this.lbSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSteps.FormattingEnabled = true;
            this.lbSteps.HorizontalScrollbar = true;
            this.lbSteps.ItemHeight = 18;
            this.lbSteps.Location = new System.Drawing.Point(23, 141);
            this.lbSteps.Margin = new System.Windows.Forms.Padding(10);
            this.lbSteps.Name = "lbSteps";
            this.lbSteps.Size = new System.Drawing.Size(400, 256);
            this.lbSteps.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 99);
            this.label2.Margin = new System.Windows.Forms.Padding(10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 22);
            this.label2.TabIndex = 11;
            this.label2.Text = "Žingsniai";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAddStep
            // 
            this.btnAddStep.AutoSize = true;
            this.btnAddStep.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAddStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddStep.Location = new System.Drawing.Point(553, 421);
            this.btnAddStep.Margin = new System.Windows.Forms.Padding(10);
            this.btnAddStep.Name = "btnAddStep";
            this.btnAddStep.Size = new System.Drawing.Size(109, 28);
            this.btnAddStep.TabIndex = 12;
            this.btnAddStep.Text = "Pridėti žingsnį";
            this.btnAddStep.UseVisualStyleBackColor = false;
            this.btnAddStep.Click += new System.EventHandler(this.BtnAddStep_Click);
            // 
            // lblData
            // 
            this.lblData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(549, 339);
            this.lblData.Margin = new System.Windows.Forms.Padding(10);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(103, 22);
            this.lblData.TabIndex = 13;
            this.lblData.Text = "Duomenys";
            this.lblData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblData.Visible = false;
            // 
            // txtData
            // 
            this.txtData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtData.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(553, 376);
            this.txtData.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(308, 24);
            this.txtData.TabIndex = 14;
            this.txtData.Visible = false;
            // 
            // btnGenerateTestCode
            // 
            this.btnGenerateTestCode.AutoSize = true;
            this.btnGenerateTestCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnGenerateTestCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateTestCode.Location = new System.Drawing.Point(116, 417);
            this.btnGenerateTestCode.Margin = new System.Windows.Forms.Padding(10);
            this.btnGenerateTestCode.Name = "btnGenerateTestCode";
            this.btnGenerateTestCode.Size = new System.Drawing.Size(188, 32);
            this.btnGenerateTestCode.TabIndex = 15;
            this.btnGenerateTestCode.Text = "Sugeneruoti testą";
            this.btnGenerateTestCode.UseVisualStyleBackColor = false;
            this.btnGenerateTestCode.Click += new System.EventHandler(this.BtnGenerateTestCode_Click);
            // 
            // txtCheckURL
            // 
            this.txtCheckURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCheckURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCheckURL.Location = new System.Drawing.Point(553, 300);
            this.txtCheckURL.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.txtCheckURL.Name = "txtCheckURL";
            this.txtCheckURL.Size = new System.Drawing.Size(308, 24);
            this.txtCheckURL.TabIndex = 17;
            this.txtCheckURL.Visible = false;
            // 
            // lblCheckURL
            // 
            this.lblCheckURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCheckURL.AutoSize = true;
            this.lblCheckURL.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckURL.Location = new System.Drawing.Point(549, 263);
            this.lblCheckURL.Margin = new System.Windows.Forms.Padding(10);
            this.lblCheckURL.Name = "lblCheckURL";
            this.lblCheckURL.Size = new System.Drawing.Size(157, 22);
            this.lblCheckURL.TabIndex = 16;
            this.lblCheckURL.Text = "URL patikrinimui";
            this.lblCheckURL.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCheckURL.Visible = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(549, 179);
            this.label3.Margin = new System.Windows.Forms.Padding(10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 22);
            this.label3.TabIndex = 18;
            this.label3.Text = "Raktažodis";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(551, 221);
            this.label4.Margin = new System.Windows.Forms.Padding(10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 22);
            this.label4.TabIndex = 19;
            this.label4.Text = "Funkcija";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDeleteLastStep
            // 
            this.btnDeleteLastStep.AutoSize = true;
            this.btnDeleteLastStep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnDeleteLastStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteLastStep.Location = new System.Drawing.Point(682, 421);
            this.btnDeleteLastStep.Margin = new System.Windows.Forms.Padding(10);
            this.btnDeleteLastStep.Name = "btnDeleteLastStep";
            this.btnDeleteLastStep.Size = new System.Drawing.Size(185, 28);
            this.btnDeleteLastStep.TabIndex = 20;
            this.btnDeleteLastStep.Text = "Pašalinti paskutinį žingsnį";
            this.btnDeleteLastStep.UseVisualStyleBackColor = false;
            this.btnDeleteLastStep.Click += new System.EventHandler(this.BtnDeleteLastStep_Click);
            // 
            // TestCaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 486);
            this.Controls.Add(this.btnDeleteLastStep);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtCheckURL);
            this.Controls.Add(this.lblCheckURL);
            this.Controls.Add(this.btnGenerateTestCode);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.btnAddStep);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbSteps);
            this.Controls.Add(this.txtXPath);
            this.Controls.Add(this.lblXPath);
            this.Controls.Add(this.cbFunction);
            this.Controls.Add(this.cbKeyword);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.lblURL);
            this.Controls.Add(this.lblUserStory);
            this.Controls.Add(this.label1);
            this.Name = "TestCaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GenerateTestCaseForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserStory;
        private System.Windows.Forms.Label lblURL;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.ComboBox cbKeyword;
        private System.Windows.Forms.ComboBox cbFunction;
        private System.Windows.Forms.Label lblXPath;
        private System.Windows.Forms.TextBox txtXPath;
        private System.Windows.Forms.ListBox lbSteps;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddStep;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Button btnGenerateTestCode;
        private System.Windows.Forms.TextBox txtCheckURL;
        private System.Windows.Forms.Label lblCheckURL;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnDeleteLastStep;
    }
}
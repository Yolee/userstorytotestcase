﻿using System;
using System.IO;
using System.Windows.Forms;

namespace UserStoryToTestCase
{
    public partial class GeneratedTestForm : Form
    {
        private readonly string _generatedTestCode;
        private readonly TestCaseForm _testCaseForm;
        private readonly string _generatedTestCase;
        private readonly GeneratedTestCaseManager _generatedTestCaseManager;

        public GeneratedTestForm(string generatedTestCode, string generatedTestCase, TestCaseForm testCaseForm)
        {
            InitializeComponent();
            _generatedTestCode = generatedTestCode;
            _generatedTestCase = generatedTestCase;
            _generatedTestCaseManager = new GeneratedTestCaseManager();
            _testCaseForm = testCaseForm;
            rtbGeneratedTestCode.Text = _generatedTestCode;
            rtbGeneratedTestCase.Text = _generatedTestCase;

        }

        private void BtnBackToTestCase_Click(object sender, EventArgs e)
        {
            _testCaseForm.EditLastTestCase();
            _testCaseForm.Show();
            this.Close();
        }

        private void BtnDownloadTest_Click(object sender, EventArgs e)
        {
            _generatedTestCaseManager.SaveFile(_generatedTestCode);
        }

        private void BtnStartNewTestCase_Click(object sender, EventArgs e)
        {
            _testCaseForm.ClearAllInputs();
            _testCaseForm.UpdateKeywordOptions();
            _testCaseForm.Show();
            this.Close();
        }

        private void BtnStartNewUserStory_Click(object sender, EventArgs e)
        {
            UserStoryForm userStoryForm = new UserStoryForm();
            userStoryForm.Show();
            _testCaseForm.Close();
            this.Close();
        }

        private void btnSaveTest_Click(object sender, EventArgs e)
        {
            _generatedTestCaseManager.SaveTestFile(_generatedTestCase);
        }
    }
}

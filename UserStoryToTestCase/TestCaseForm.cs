﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UserStoryToTestCase
{
    public partial class TestCaseForm : Form
    {
        private readonly string _userStory;
        private TestCaseManager _testCaseManager;

        public TestCaseForm(string userStory)
        {
            InitializeComponent();
            _userStory = userStory;
            lblUserStory.Text = _userStory;
            cbKeyword.SelectedIndex = 0;
            cbFunction.SelectedIndex = 0;
            cbFunction.SelectedIndexChanged += CbFunction_SelectedIndexChanged;

            _testCaseManager = new TestCaseManager
            {
                UserStory = _userStory
            };
        }

        private void BtnAddStep_Click(object sender, EventArgs e)
        {
            UpdateTestCaseManagerFromUI();

            if (!_testCaseManager.IsStepValid())
            {
                MessageBox.Show("Užpildykite visus laukus prieš pridedant žingsnį.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (_testCaseManager.IsFirstStep(lbSteps.Items.Count))
            {
                lbSteps.Items.Add(_testCaseManager.AddFirstStep(lbSteps.Items.Count));
            }

            string step = _testCaseManager.GenerateStep();
            lbSteps.Items.Add(step);

            string lastKeyword = lbSteps.Items.Cast<string>().Last().Split(' ')[0];
            UpdateKeywordOptions(lastKeyword);
        }

        private void BtnGenerateTestCode_Click(object sender, EventArgs e)
        {
            if (!_testCaseManager.IsTestCaseValid(cbKeyword.Items[0].ToString()))
            {
                MessageBox.Show("Testo atvėjis neatitinka visiems testinio atvejo reikalavimams.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string generatedTestCode = _testCaseManager.GenerateTestCode(txtURL.Text, lbSteps.Items);
            string generatedTestCase = _testCaseManager.GenerateTestCasesList(lbSteps.Items);

            GeneratedTestForm generatedTestForm = new GeneratedTestForm(generatedTestCode, generatedTestCase, this);
            generatedTestForm.Show();
            this.Hide();
        }

        private void BtnDeleteLastStep_Click(object sender, EventArgs e)
        {
            if (_testCaseManager.DeleteLastStep(lbSteps.Items))
            {
                string lastKeyword = lbSteps.Items.Count > 0 ? lbSteps.Items.Cast<string>().Last().Split(' ')[0] : string.Empty;
                UpdateKeywordOptions(lastKeyword);
            }
            else
            {
                MessageBox.Show("Neliko žingsnių ištrinimui.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }   
        }

        private void UpdateControlVisibility()
        {
            string function = cbFunction.SelectedItem.ToString();

            lblXPath.Visible = function == "Press Button" || function == "Fill the Field" || function == "Check output";
            txtXPath.Visible = lblXPath.Visible;

            lblData.Visible = function == "Fill the Field" || function == "Check output";
            txtData.Visible = lblData.Visible;

            lblCheckURL.Visible = function == "Check URL";
            txtCheckURL.Visible = lblCheckURL.Visible;
        }

        public void UpdateKeywordOptions(string lastKeyword = null)
        {
            if (lastKeyword == null)
            {
                cbKeyword.Items.Clear();
                cbKeyword.Items.AddRange(new object[] { "WHEN", "AND" });
                cbKeyword.SelectedIndex = 0;
                return;
            }

            lastKeyword = lastKeyword ?? lbSteps.Items.Cast<string>().Last().Split(' ')[0];

            if (lastKeyword == "GIVEN")
            {
                cbKeyword.Items.Clear();
                cbKeyword.Items.AddRange(new object[] { "WHEN", "AND" });
                cbKeyword.SelectedIndex = 0;
            }
            else if (lastKeyword == "WHEN")
            {
                cbKeyword.Items.Clear();
                cbKeyword.Items.AddRange(new object[] { "THEN", "AND" });
                cbKeyword.SelectedIndex = 0;
            }
            else if (lastKeyword == "THEN")
            {
                cbKeyword.Items.Clear();
                cbKeyword.Items.Add("AND");
                cbKeyword.SelectedIndex = 0;
            }
        }

        private void UpdateTestCaseManagerFromUI()
        {
            _testCaseManager.Url = txtURL.Text;
            _testCaseManager.Keyword = cbKeyword.SelectedItem.ToString();
            _testCaseManager.Function = cbFunction.SelectedItem.ToString();
            _testCaseManager.XPath = txtXPath.Text;
            _testCaseManager.Data = txtData.Text;
            _testCaseManager.CheckURL = txtCheckURL.Text;
        }

        public void ClearAllInputs()
        {
            txtURL.Clear();
            txtXPath.Clear();
            txtData.Clear();
            txtCheckURL.Clear();
            lbSteps.Items.Clear();
        }

        private void CbFunction_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateControlVisibility();
        }

        public void EditLastTestCase()
        {
            _testCaseManager.RemoveLastTestCase();
        }
    }
}
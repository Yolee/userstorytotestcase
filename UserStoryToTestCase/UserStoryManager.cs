﻿namespace UserStoryToTestCase
{
    public class UserStoryManager
    {
        public bool AreInputsValid(string role, string action, string outcome)
        {
            return !string.IsNullOrWhiteSpace(role) &&
                   !string.IsNullOrWhiteSpace(action) &&
                   !string.IsNullOrWhiteSpace(outcome);
        }

        public string CreateUserStory(string role, string action, string outcome)
        {
            return $"Kaip {role}, Aš noriu {action}, kad {outcome}.";
        }
    }
}


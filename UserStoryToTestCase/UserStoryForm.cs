﻿using System;
using System.Windows.Forms;

namespace UserStoryToTestCase
{
    public partial class UserStoryForm : Form
    {
        private readonly UserStoryManager _userStoryManager;

        public UserStoryForm()
        {
            InitializeComponent();
            _userStoryManager = new UserStoryManager();
        }

        private void BtnSaveAndContinue_Click(object sender, EventArgs e)
        {
            string role = txtRole.Text;
            string action = txtAction.Text;
            string outcome = txtOutcome.Text;

            if (_userStoryManager.AreInputsValid(role, action, outcome))
            {
                string userStory = _userStoryManager.CreateUserStory(role, action, outcome);
                var testCaseForm = new TestCaseForm(userStory);
                testCaseForm.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Užpildykite visus laukus.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

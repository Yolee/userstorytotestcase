﻿using NUnit.Framework;
using UserStoryToTestCase;

namespace UserStoryToTestCaseTests
{
    [TestFixture]
    public class UserStoryManagerTests
    {
        private UserStoryManager _userStoryManager;

        [SetUp]
        public void Setup()
        {
            _userStoryManager = new UserStoryManager();
        }

        [Test]
        public void AreInputsValid_ShouldReturnTrue_WhenAllInputsAreValid()
        {
            bool result = _userStoryManager.AreInputsValid("User", "Login", "View dashboard");
            Assert.IsTrue(result);
        }

        [Test]
        public void AreInputsValid_ShouldReturnFalse_WhenRoleIsEmpty()
        {
            bool result = _userStoryManager.AreInputsValid("", "Login", "View dashboard");
            Assert.IsFalse(result);
        }

        [Test]
        public void AreInputsValid_ShouldReturnFalse_WhenActionIsEmpty()
        {
            bool result = _userStoryManager.AreInputsValid("User", "", "View dashboard");
            Assert.IsFalse(result);
        }

        [Test]
        public void AreInputsValid_ShouldReturnFalse_WhenOutcomeIsEmpty()
        {
            bool result = _userStoryManager.AreInputsValid("User", "Login", "");
            Assert.IsFalse(result);
        }

        [Test]
        public void CreateUserStory_ShouldReturnCorrectUserStoryString_WhenAllInputsAreValid()
        {
            string userStory = _userStoryManager.CreateUserStory("User", "Login", "View dashboard");
            Assert.AreEqual("Kaip User, Aš noriu Login, kad View dashboard.", userStory);
        }
    }
}
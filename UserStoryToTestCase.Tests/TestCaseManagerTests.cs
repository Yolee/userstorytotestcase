﻿using NUnit.Framework;
using UserStoryToTestCase;

namespace UserStoryToTestCaseTests
{
    public class TestCaseManagerTests
    {
        private TestCaseManager _testCaseManager;

        [SetUp]
        public void Setup()
        {
            _testCaseManager = new TestCaseManager();
        }

        [Test]
        public void IsValidTestCase_ShouldReturnTrue_WhenTestCaseIsValid()
        {
            _testCaseManager.AddStep("GIVEN I am on the URL \"https://example.com\"");
            _testCaseManager.AddStep("WHEN I fill the field with XPath: \"//input[@type='text']\" with \"username\"");
            _testCaseManager.AddStep("AND I press the button with XPath: \"//input[@type='submit']\"");
            _testCaseManager.AddStep("THEN I check URL \"https://example.com/dashboard\"");

            bool result = _testCaseManager.IsTestCaseValid("AND");
            Assert.IsTrue(result);
        }

        [Test]
        public void IsValidTestCase_ShouldReturnFalse_WhenTestCaseIsInvalid()
        {
            _testCaseManager.AddStep("GIVEN I am on the URL \"https://example.com\"");
            _testCaseManager.AddStep("WHEN I fill the field with XPath: \"//input[@type='text']\" with \"username\"");

            bool result = _testCaseManager.IsTestCaseValid("THEN");
            Assert.IsFalse(result);
        }

        [Test]
        public void AddStep_ShouldIncreaseStepCount()
        {
            int initialStepCount = _testCaseManager.GetStepCount();
            _testCaseManager.AddStep("GIVEN I am on the URL \"https://example.com\"");
            Assert.AreEqual(initialStepCount + 1, _testCaseManager.GetStepCount());
        }

        [Test]
        public void RemoveLastStep_ShouldDecreaseStepCount()
        {
            _testCaseManager.AddStep("GIVEN I am on the URL \"https://example.com\"");
            int initialStepCount = _testCaseManager.GetStepCount();
            _testCaseManager.DeleteLastStep(_testCaseManager.GetSteps());
            Assert.AreEqual(initialStepCount - 1, _testCaseManager.GetStepCount());
        }

        [Test]
        public void RemoveLastStep_ShouldNotDecreaseStepCount_WhenNoSteps()
        {
            int initialStepCount = _testCaseManager.GetStepCount();
            _testCaseManager.DeleteLastStep(_testCaseManager.GetSteps());
            Assert.AreEqual(initialStepCount, _testCaseManager.GetStepCount());
        }
    }
}
